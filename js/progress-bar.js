$(document).ready(function () {
    
    $(window).scroll(function () {
        var s = $(this).scrollTop(),
            d = $(document).height()-$(window).height(),
            scrollPercent = (s / d)*100 + '%';       
        $("#progressbar").css('width', scrollPercent);
     });
    
 });