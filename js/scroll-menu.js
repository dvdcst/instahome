$(document).ready(function(){
    
    $('img.logo-black').hide();
    
    $(window).scroll( function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            $('#header').addClass('fixed-menu-mobile');
            $('img.logo-black').show();
            $('img.logo-white').hide();
            $('#nav ul li a').addClass('padding-scroll-menu');
            $('#nav').addClass('margin-scroll-menu');
        } else {
            $('#header').removeClass('fixed-menu-mobile');
            $('img.logo-black').hide();
            $('img.logo-white').show();
            $('#nav ul li a').removeClass('padding-scroll-menu');
            $('#nav').removeClass('margin-scroll-menu');
        }
    });
    
});